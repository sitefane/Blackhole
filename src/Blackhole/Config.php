<?php
namespace DigitalTurbo\BlackHole;

use Monolog\Logger;

class Config
{
    const BASE_URL = "http://blackhole.digiturbo.cn/api/v1";

    public static function back_log_level(string $logStr)
    {
        $level = array('DEBUG' => Logger::DEBUG,
                       'INFO'                 => Logger::INFO,
                       'NOTICE'               => Logger::NOTICE,
                       'WARNING'              => Logger::WARNING,
                       'ERROR'                => Logger::ERROR,
                       'CRITICAL'             => Logger::CRITICAL,
                       'ALERT'                => Logger::ALERT,
                       'EMERGENCY'            => Logger::EMERGENCY);
        return $level[$logStr] ?? Logger::ERROR;
    }
}
