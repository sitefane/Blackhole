<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/29
 * Time: 6:15 PM
 */

namespace DigitalTurbo\BlackHole;

use DigitalTurbo\BlackHole\Method\MsgSend;


/**
 * Class Piston
 * @method Piston init(string $appId,string $appSecret,$logPath ='',$logLevel='')
 * @method Piston send(string $url,array $userArr)
 * @method Piston go(bool $async)
 * @package DigitalTurbo
 */
class Piston
{
    protected $method;
    protected static $instance = null;
    protected $kernel;

    public function __construct() {
        $this->method =  new MsgSend($this);
        $this->kernel =  new Kernel($this);
    }

    public function __call($name, $arguments)
    {
        if(method_exists($this->method,$name)){
            $result = $this->method->$name(...$arguments);
        }else{
            $result = $this->kernel->getService($name)->call($this,...$arguments);
        }
        return $result;
    }

    public static function __callStatic($name, $arguments)
    {
        $instance = new self();
        return $instance->$name(...$arguments);
    }

}
