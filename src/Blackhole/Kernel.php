<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/30
 * Time: 12:12 AM
 */

namespace DigitalTurbo\BlackHole;

use Closure;

class Kernel
{
    protected $binds;
    protected $piston;

    public function __construct(Piston $ql)
    {
        $this->piston = $ql;
        $this->binds  = [];
    }

    public function bind(string $name, Closure $provider)
    {
        $this->binds[$name] = $provider;
    }

    public function getService(string $name)
    {
        if (!($this->binds[$name] ?? "")) {
            throw new \Exception("Service: {$name} not found!");
        }
        return $this->binds[$name];
    }

    public function bootstrap()
    {
        return $this;
    }
//    private function register(ServiceProviderContract $instance)
    //    {
    //        $instance->register($this);
    //    }
}
