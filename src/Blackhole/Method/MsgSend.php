<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/30
 * Time: 12:20 AM
 */

namespace DigitalTurbo\BlackHole\Method;

use DigitalTurbo\BlackHole\Config;
use DigitalTurbo\BlackHole\Contracts\Request;
use DigitalTurbo\BlackHole\Piston;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class MsgSend
 * @package DigitalTurbo\BlackHole\Method
 */
class MsgSend extends Request
{
    protected $appId;
    protected $appSecret;
    protected $piston;
    protected $url;

    /**
     * @var  array
     */
    protected $data = [];

    public function init($appId, $appSecret, $logPath = '', $logLevel = '')
    {
        $data['appconfig']['appid']     = $appId;
        $data['appconfig']['appsecret'] = $appSecret;
        $this->data                     = $data;
        if (!empty($logPath) && !empty($logLevel)){
            $this->log->pushHandler(new StreamHandler($logPath, Config::back_log_level($logLevel)));
        }
        return $this->piston;
    }

    public function __construct(Piston $piston)
    {
        $this->piston = $piston;
        $this->log    = new Logger(__CLASS__);
    }

    public function send(string $url, array $userArr)
    {
        $this->url         = $url;
        $this->data['arr'] = $userArr;
        return $this->piston;
    }

    public function go($async = true)
    {
        if ($async) {
            return $this->AsyncCurl($this->url, $this->data);
        }
        return $this->curl($this->url, $this->data);
    }
}
