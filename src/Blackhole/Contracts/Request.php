<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/29
 * Time: 2:21 PM
 */

namespace DigitalTurbo\BlackHole\Contracts;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;


abstract class Request implements MethodContracts
{
    public $log;

    public function AsyncCurl(string $url, array $data, $method = "POST",$headers = [],$verify = false)
    {
        $client = new Client();
        $promise = $client->requestAsync($method, $url, [
            'verify'=>$verify,
            'headers'=>$headers,
            'form_params'=> $data
        ])->then(
            function (ResponseInterface $res) {
                $this->log->debug('send message success', json_decode($res->getBody(),true));
                echo $res->getBody() . "\n";
            },
            function (RequestException $e) {
                $this->log->error('commit data error', $e);
                echo $e->getMessage() . "\n";
                echo $e->getRequest()->getMethod();
            }
        );
        $promise->wait();
    }

    public function curl(string $url, array $data, $method = "POST",$headers = [],$verify = false)
    {
        $client = new Client();
        try {
            $res = $client->request($method, $url, [
                'verify'=>$verify,
                'headers'=>$headers,
                'form_params'=> $data
            ]);
            if ($res->getStatusCode() == "200") {
                $this->log->debug('send message success', $data);
                return $res->getBody()->getContents();
            } else {
                $this->log->error('commit data error', $data);
                return $res->getBody()->getContents();
            }
        } catch (RequestException $e) {
            $this->log->error('request GuzzleException', array('request' => Psr7\str($e->getRequest()), 'response' => Psr7\str($e->getResponse())));
            return json_encode(array('success' => false, 'msg' => 'request GuzzleException'));
        }
    }
}
