<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/30
 * Time: 12:11 AM
 */

namespace DigitalTurbo\BlackHole\Contracts;


use DigitalTurbo\BlackHole\Kernel;

interface ServiceProviderContract
{
    public function register(Kernel $kernel);
}