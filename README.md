
### 安装

```
composer require digitalturbo/blackhole
```

#### 示例
```
use DigitalTurbo\BlackHole\Piston;

$url = "http://blackhole.digiturbo.cn/api/v1/message/tpsend';
$piston = new Piston();
$res    = $model->init($appId,$appSecret, $this->logPath, $debugType)
            ->send($url, ['321', '321'])
            ->go(false);


```
### 方法

#### @method `init`(string $appId,string $appSecret,$logPath,$logLevel) 

**所有参数不能为空**

参数|说明
:----|:----:
$appId| 小程序appid 
$appSecret|小程序密钥
$logPath|日志文件路径
$logLevel|日志级别 默认 ERROR

#### @method `send`(string $url,array $userArr)

**所有参数不能为空**

参数|说明
:----|:----:
$url| url
$userArr| 参数数据

#### @method `go`(bool $async)

当 $async 为真时，

参数|是否有默认值|说明
:----|:----:|:----:
$async = true| true | 异步执行请求，默认值;`无返回值`
$async = false| false | `有返回值，返回请求信息`