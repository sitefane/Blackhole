<?php
/**
 * Created by PhpStorm.
 * User: umono
 * Date: 2019/1/29
 * Time: 11:27 AM
 */

namespace DigitalTurbo\Tests\Send;

use DigitalTurbo\BlackHole\Config;
use DigitalTurbo\BlackHole\Piston;
use PHPUnit\Framework\TestCase;

class PistonTest extends TestCase
{
//    public $logPath;
//
//    public function setUp()
//    {
//        $logPath       = __DIR__ . '/piston.log';
//        $this->logPath = $logPath;
//        $fp            = fopen($logPath, "a");
//        fwrite($fp, PHP_EOL);
//        @chmod(__DIR__, 0777);
//        fclose($fp);
//    }

    public function testService()
    {
        $url   = Config::BASE_URL;
        $model = new Piston();
        $res   = $model->init('1', '2')
            ->send($url . '/message/tpsend', ['321', '321'])
            ->go(true);
        $this->assertEquals(
            false,
            json_decode($res, true)['success']
        );
    }
}
